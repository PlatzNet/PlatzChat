(function($) {
    var socket = io();

    console.log("Initing OpenPGP worker...");
    openpgp.initWorker({ path:'/js/openpgp.worker.min.js' });

    console.log("Generating PGP keypair...");
    $('.loader .loader_label').html("Generating PGP keypair...");

    openpgp.generateKey({
        userIds: [{}],
        numBits: 4096
    }).then(function(key) {
        var priv = key.privateKeyArmored;
        var pub = key.publicKeyArmored;

        console.log(priv);
        console.log();
        console.log(pub);
        console.log("Generated!!");
        console.log("Receiving username...");
        $('.loader .loader_label').html("Receiving username...");

        $.get("/api/get-username", function(usernameData) {
            var username = usernameData.username;
            console.log("Username: " + username);
            console.log("Registering...");
            $('.loader .loader_label').html("Authenticating...");
            $.post("/api/register-username", {
                username: username,
                key: pub
            }, function(cipherData) {
                var cipher = cipherData.cipher;
                console.log("Cipher: " + cipher);
                console.log("Decrypting...");
                $('.loader .loader_label').html("Logging in...");
                openpgp.decrypt({
                    message: openpgp.message.readArmored(cipher),
                    privateKey: openpgp.key.readArmored(priv).keys[0]
                }).then(function(checkStr) {
                    console.log("Decrypted: " + checkStr.data);
                    socket.emit("login", {
                        username: username,
                        cipher: checkStr.data,
                        key: pub
                    });

                    socket.on("err", function(data) {
                        console.log(data.error);
                    });

                    socket.on("success", function(data) {
                        switch(data.success) {
                            case "login-succeeded":
                                if(data.roomName) {
                                    var room = data.roomName;

                                    $('.loader_figure').toggleClass('stop', true);
                                    $('.loader_label').fadeOut();
                                    $('.join .off').toggleClass('off', false);

                                    $('.join button')
                                        .html(room)
                                        .click(function() {
                                            console.log("joining... " + room);
                                            socket.emit("join", {
                                                roomName: room.toString()
                                            });
                                        });
                                    $('.join input').keypress(function() {
                                        if($('.join input').val().length == 36) {
                                            console.log("joining..." + room);
                                            socket.emit("join", {
                                                roomName: $('.join input').val()
                                            });
                                        }
                                    });
                                }
                                break;
                        }
                    });

                    socket.on("joined", function(data) {
                        console.log(data);
                        if(data.roomName) {
                            $('.join').fadeOut();
                        }
                    });
                })
            });
        });
    });
})(jQuery);