module.exports = exports = {
    userName: function(len) {
        var vocals     = "aeiou",
            consonants = "bcdfghjklmnpqrstvwxyz";

        var iterations = (len + len % 2) / 2;

        var str = "";

        for(var i = 0; i < iterations; i++) {
            str += consonants[Math.floor(Math.random() * consonants.length)];
            str += vocals[Math.floor(Math.random() * vocals.length)];
        }

        if(len % 2 == 1)
            str = str.slice(0, -1);

        return str;
    },
    checkString: function() {
        return Math.floor((1 + Math.random()) * 0x100000000).toString(16).substring(1);
    },
    roomName: function() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }
};