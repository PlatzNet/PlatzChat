var redis = require('redis'),
    client = redis.createClient();

var bodyParser = require('body-parser'),
    express = require('express'),
    app = express(),
    http = require('http'),
    server = http.createServer(app);

var openpgp = require('openpgp'),
    io = require('socket.io')(server),
    sockets = {};

var random = require('./libs/random');

var userPrefix = "USR_", // TODO: Add this to a config.
    roomPrefix = "CHT_";

client.on("error", function(err) {
    console.log("ERROR: " + err);
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/views/index.html');
});

app.use('/js', express.static(__dirname + '/static/js'));
app.use('/img', express.static(__dirname + '/static/img'));
app.use('/css', express.static(__dirname + '/static/css'));

app.get('/api/:key', function(req, res) {
    switch(req.params.key) {
        case "get-username":
            var username = random.userName(10); // TODO: Add this to a config.

            res.json({
                username: username
            });
            break;
    }
});

app.get('/api/:key/:value', function(req, res) {
    switch(req.params.key) {
        case "username-exists":
            client.exists(userPrefix + req.params.value, function(err, exists) {
                if(err)
                    return res.json({
                        error: "internal-server-error"
                    });

                if(exists == 0)
                    return res.json({
                        exists: false
                    });

                else if(exists == 1)
                    return res.json({
                        exists: true
                    });
            });
            break;
    }
});

app.get('/api/:key/:value/:subvalue', function(req, res) {
    switch(req.params.key) {
        case "activate-username":
            client.exists(userPrefix + req.params.value, function(err, exists) {
                if(err)
                    return res.json({
                        error: "internal-server-error"
                    });

                if(exists == 1) {
                    client.get(userPrefix + req.params.value, function(err, result) {
                        if(result == req.params.subvalue) {
                            return res.json({
                                success: true
                            });
                        } else return res.json({
                            success: false
                        });
                    });
                } else if(exists == 0)
                    return res.json({
                        error: "username-not-existing"
                    });
                else return res.json({
                        error: "internal-server-error"
                    });
            });
            break;
    }
});

app.post('/api/:key', function(req, res) {
    switch(req.params.key) {
        case "register-username":
            if(req.body.username && req.body.key) {
                client.exists(userPrefix + req.body.username, function(err, exists) {
                    if(exists)
                        return res.json({
                            error: "already-exists"
                        });

                    var checkStr = random.checkString();
                    console.log(checkStr);
                    client.set(userPrefix + req.body.username, checkStr, function(err, result) {
                        if(err)
                            return res.json({
                                error: "internal-server-error"
                            });

                        openpgp.encrypt({
                            data: checkStr,
                            publicKeys: openpgp.key.readArmored(req.body.key).keys
                        }).then(function(ciphertext) {
                            res.json({
                                cipher: ciphertext.data
                            });
                        });
                    });
                });
            }
            break;
    }
});

io.on('error', function(err) {
    console.log(err);
});

io.on('connection', function(socket) {
    var uname,
        key;

    socket.on("login", function(data) {
        if(!(data.username && data.key && data.cipher))
            return socket.emit("err", {
                error: "err-request"
            });

        uname = data.username;
        key = data.key;

        client.exists(userPrefix + data.username, function(err, exists) {
            if(err)
                return socket.emit("err", {
                    error: "internal-server-error"
                });

            if(exists == 1) {
                client.get(userPrefix + data.username, function(err, result) {
                    if(result == data.cipher) {
                        client.set(userPrefix + data.username, data.key, function(err, result) {
                            if(err)
                                return socket.emit("err", {
                                    error: "internal-server-error"
                                });

                            findRoom(function(roomName) {
                                if(roomName) {
                                    console.log(roomName);
                                    socket.emit("success", {
                                        success: "login-succeeded",
                                        roomName: roomName
                                    });
                                    sockets[data.username] = socket;
                                }
                                else socket.emit("err", {
                                    error: "internal-server-error"
                                });
                            });
                        });
                    } else return socket.emit("err", {
                        error: "login-failed"
                    });
                });
            } else if(exists == 0)
                return socket.emit("err", {
                    error: "username-not-existing"
                });
            else return socket.emit("err", {
                    error: "internal-server-error"
                });
        });

        socket.on("join", function(data) {
            if(!data.roomName)
                return socket.emit("err", {
                    error: "invalid-request"
                });

            client.rpush(roomPrefix + data.roomName, uname, function(err) {
                if(err)
                    return socket.emit("err", {
                        error: "internal-server-error"
                    });

                return socket.emit("joined", {
                    roomName: data.roomName
                });
            });
        });
    });
});

function findRoom(callback) {
    var roomName = random.roomName();
    client.exists(roomPrefix + roomName, function(err, exists) {
        if(err)
            callback(false);

        if(exists == 1)
            return findRoom(callback);
        else if(exists == 0) {
            callback(roomName);
        }
    });
}

server.listen(8080); // TODO: Add this to a config.